import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import * as math from 'mathjs';
import { lookup } from 'wordnet';
import { Wit } from 'node-wit';
import { delay, map } from 'rxjs/operators';
import { pipe } from 'rxjs';

declare var fs: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  @ViewChild('chatContainer', { static: true }) el: ElementRef;

  constructor(private http: HttpClient) {}
  title = 'chatbot';
  question: string;
  messages = [];
  loading: boolean = true;

  ngOnInit() {
    this.pingServer().subscribe((data) => {
      console.log(data);
      this.loading = false;
      this.initialise();
    });
  }

  initialise() {
    setTimeout(() => {
      this.updateMessage('hi i am Tweak !', false);
      setTimeout(() => {
        this.updateMessage(
          'i can solve differentiations, convert units, find meanings',
          false
        );
      }, 500);
    }, 500);
  }

  mainFunction(param?) {
    if (this.question?.length) {
      this.updateMessage(this.question, true);
      this.getWitResponse(this.question).subscribe((response) => {
        console.log('got wit response');
        this.updateMessage(response['message'], false);
      });
      this.question = '';
    }
  }

  getWitResponse(query) {
    return this.http.post(
      `https://infinite-ocean-86146.herokuapp.com/message`,
      { message: query }
    );
  }

  updateMessage(text, human) {
    this.messages.push({
      human: human,
      text: text.replaceAll('\n', '<br/>'),
      time: `${new Date().getHours()}:${new Date().getMinutes()}`,
    });
    setTimeout(() => {
      this.el.nativeElement.scrollTop = this.el.nativeElement.scrollHeight;
    });
  }

  pingServer() {
    return this.http.get(`https://infinite-ocean-86146.herokuapp.com/`);
  }
}
